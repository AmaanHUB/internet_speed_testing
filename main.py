#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

#  import the speedtest library
import speedtest

# used to get the path of the results file
import os

# allows manipulating csv files
import csv

# allows getting time and date
from datetime import date, datetime

# declare speedtest variable
st = speedtest.Speedtest()

download_speed = 0
upload_speed = 0
ping = 0

# used in converting bps to Mbps
mega =  1000000

# variable with the download speed in Mbps, rounded to 2dp
download_speed = round(st.download() / mega, 2)

# variable with the download speed in Mbps, rounded to 2dp
upload_speed = round(st.upload() / mega, 2)

# show the ping, rounded to 2dp
ping = round(st.results.ping, 2)

# get current date
today = date.today()
date = today.strftime("%d/%m/%Y")

# get current time
now = datetime.now()
current_time = now.strftime("%H:%M:%S")

# function to write the results to a csv file
# This could be done with the sniffer function in csv (to see if the first line is a header),
# but then I would have to make a weird to conditional as to create the file or not. This way
# is more elegant probably
def write_to_csv():
    # if the file does not exist then do ...
    if os.path.exists("./results.csv") ==  False:
        with open("./results.csv", "a+", encoding="UTF8") as results:
            # create the csv writer
            writer = csv.writer(results)
            # headings of the columns
            headings = ['Download (Mbps)', 'Upload (Mbps)', 'Ping (ms)', 'Date (DD/MM/YYYY)', 'Time']
            writer.writerow(headings)
            # write a row to the csv file
            writer.writerow([download_speed, upload_speed, ping, date, current_time])
    else:
        # find a way to stop this bit from repeating
        with open("./results.csv", "a+", encoding="UTF8") as results:
         # create the csv writer
            writer = csv.writer(results)
            writer.writerow([download_speed, upload_speed, ping, date, current_time])

# print out csv file, only really used for debugging purposes, hence why everything is hard-coded
def print_csv():
    with open("./results.csv", "r", encoding="UTF8") as results:
        reader = csv.reader(results)
        for row in reader:
            print(" ".join(row))


write_to_csv()
# print_csv()

