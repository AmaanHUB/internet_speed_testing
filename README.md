# Internet Speed Testing

The idea of this project is to create a Python script that will test the internet upload/download speed (and ping) and then put this into a csv file that can then be read on a graph. This will be run at regular intervals using cron.


Possibly will be put into a docker container too using a Dockerfile (and docker-compose maybe)

## TODO
- [ ] Need to implement the graph part maybe
- [ ] Docker container of this, though it seems to be working fine without
  - [ ] ITERATION - create a docker-compose file
- [ ] ITERATION - consider using a database instead of csv file, will be only done if made into a container I think
